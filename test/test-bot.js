const Server = require('scuttle-testbot')

module.exports = function (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   keys: SecretKeys
  //
  //   recpsGuard: Boolean,
  //   tribes: Boolean
  // }
  //
  const stack = Server

  stack
    .use(require('ssb-db2/core'))
    .use(require('ssb-classic'))
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/publish'))
    .use(require('ssb-db2/compat/history-stream'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-box2'))
    .use(require('ssb-tribes'))
    .use(require('../')) // ssb-settings

  if (opts.recpsGuard === true) {
    stack.use(require('ssb-recps-guard'))
  }

  const ssb = stack({
    noDefaultUse: true,
    ...opts,
    box2: {
      legacyMode: true,
      ...opts.box2
    }
  })

  return ssb
}
