module.exports = {
  type: 'link/feed-settings',
  tangle: 'link',
  staticProps: {
    parent: { type: 'string', required: true }, // FeedId
    child: { type: 'string', required: true } // SettingsId
  },
  props: {
  }
}
