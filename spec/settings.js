const Overwrite = require('@tangle/overwrite')

const overwrite = (schema) => Overwrite({ valueSchema: schema })
const boolean = overwrite({ type: 'boolean' })

module.exports = {
  type: 'settings',
  tangle: 'settings',
  props: {
    keyBackedUp: boolean
  }
}
